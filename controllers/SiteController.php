<?php

namespace app\controllers;

use app\models\Dish;
use app\models\DishIngridient;
use app\models\Ingridient;
use app\models\SignupForm;
use app\models\User;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        //getting dishes ids that have disabled ingridients
        $disabledDishIds = Ingridient::find()->select('dish_ingridient.dish_id')->joinWith('dishIngridients')->where(['ingridient.active' => Ingridient::DISABLED])->distinct()->column();
        //exclude disabled dishes from output
        $dishesQuery = Dish::find()->where(['NOT IN', 'id', $disabledDishIds]);

        if (($ids = Yii::$app->request->get('ids')) != null) {
            $allModels = [];
            if (count($ids) >= 2) {
                $dishes = $dishesQuery->all();
                $fullCoincidence = [];
                $partCoincidence = [];
                foreach ($dishes as $dish) {
                    $ingridientIds = ArrayHelper::getColumn($dish->ingridients, 'id');
                    $dish = ArrayHelper::toArray($dish);
                    $coincidence = count(array_intersect($ids, $ingridientIds));

                    $dish['coincidence'] = $coincidence;
                    if ($coincidence == count($ids)) {
                        $fullCoincidence[] = $dish;
                    } elseif ($coincidence >= 2) {
                        $partCoincidence[] = $dish;
                    }
                }
                //if exists full coincidence return them else return more than 1 coincidence
                if (!empty($fullCoincidence)) {
                    $allModels = $fullCoincidence;
                } else if(count($partCoincidence) >= 2) {
                    ArrayHelper::multisort($partCoincidence, 'coincidence', SORT_DESC);
                    $allModels = $partCoincidence;
                }
            } else {
                //if selected few than 2 ingridients
                Yii::$app->session->setFlash('warning', 'Please select more than 1 ingridients');
            }
        } else {
            $allModels = $dishesQuery->asArray()->all();
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Signup action.
     *
     * @return Response|string
     */
    public function actionSignup()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', "You're successfully registered! Please, log in.");
            return $this->redirect(['login']);
        }

        $model->password = '';
        $model->password_confirm = '';
        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
