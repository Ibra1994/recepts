<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\Ingridient;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Dish */
/* @var $form yii\widgets\ActiveForm */
$activeIngridients = Ingridient::find()->where(['active' => Ingridient::ACTIVE])->all();
?>

<div class="dish-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ingridient_ids')->widget(Select2::class, [
        'data' => ArrayHelper::map($activeIngridients, 'id', 'name'),
        'showToggleAll' => false,
        'options' => [
            'placeholder' => 'Select ingridients ...',
            'multiple' => true
        ],
        'pluginOptions' => [
            'maximumSelectionLength' => 5
        ],
    ]); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
