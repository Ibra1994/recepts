<?php

namespace app\models;

use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%ingridient}}".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $active
 * @property int $created_at
 * @property int $updated_at
 *
 * @property DishIngridient[] $dishIngridients
 * @property Dish[] $dishes
 */
class Ingridient extends \yii\db\ActiveRecord
{
    const DISABLED = 0;
    const ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ingridient}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
            ],
            TimestampBehavior::class
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['active'], 'integer'],
            [['name'], 'unique'],
            [['slug'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'slug' => Yii::t('app', 'Slug'),
            'active' => Yii::t('app', 'Active'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDishIngridients()
    {
        return $this->hasMany(DishIngridient::className(), ['ingridient_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDishes()
    {
        return $this->hasMany(Dish::className(), ['id' => 'dish_id'])->viaTable('{{%dish_ingridient}}', ['ingridient_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return IngridientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new IngridientQuery(get_called_class());
    }
}
