<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%dish_ingridient}}".
 *
 * @property int $dish_id
 * @property int $ingridient_id
 *
 * @property Dish $dish
 * @property Ingridient $ingridient
 */
class DishIngridient extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dish_ingridient}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dish_id', 'ingridient_id'], 'required'],
            [['dish_id', 'ingridient_id'], 'integer'],
            [['dish_id', 'ingridient_id'], 'unique', 'targetAttribute' => ['dish_id', 'ingridient_id']],
            [['dish_id'], 'exist', 'skipOnError' => true, 'targetClass' => Dish::className(), 'targetAttribute' => ['dish_id' => 'id']],
            [['ingridient_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ingridient::className(), 'targetAttribute' => ['ingridient_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dish_id' => Yii::t('app', 'Dish ID'),
            'ingridient_id' => Yii::t('app', 'Ingridient ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDish()
    {
        return $this->hasOne(Dish::className(), ['id' => 'dish_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngridient()
    {
        return $this->hasOne(Ingridient::className(), ['id' => 'ingridient_id']);
    }

    /**
     * @inheritdoc
     * @return DishIngridientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DishIngridientQuery(get_called_class());
    }
}
