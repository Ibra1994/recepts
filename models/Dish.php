<?php

namespace app\models;

use voskobovich\behaviors\ManyToManyBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%dish}}".
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property int $created_at
 *
 * @property DishIngridient[] $dishIngridients
 * @property Ingridient[] $ingridients
 */
class Dish extends \yii\db\ActiveRecord
{


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%dish}}';
    }


    public function behaviors()
    {
        return [
            [
                'class' => SluggableBehavior::class,
                'attribute' => 'name',
            ],
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
            ],
            [
                'class' => ManyToManyBehavior::class,
                'relations' => [
                    'ingridient_ids' => 'ingridients'
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'ingridient_ids'], 'required'],
            [['created_at'], 'integer'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['slug'], 'unique'],
            [['ingridient_ids'], 'each', 'rule' => ['integer']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'slug' => Yii::t('app', 'Slug'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDishIngridients()
    {
        return $this->hasMany(DishIngridient::className(), ['dish_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIngridients()
    {
        return $this->hasMany(Ingridient::className(), ['id' => 'ingridient_id'])->viaTable('{{%dish_ingridient}}', ['dish_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return DishQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DishQuery(get_called_class());
    }
}
