<?php

use kartik\select2\Select2;
use app\models\Ingridient;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ArrayDataProvider */

$this->title = 'My Yii Application';
$activeIngridients = Ingridient::find()->where(['active' => Ingridient::ACTIVE])->all();
?>
<div class="site-index">
    <?= Html::beginForm(['/'], 'get') ?>
    <?= Select2::widget([
        'name' => 'ids',
        'value' => Yii::$app->request->get('ids'),
        'data' => ArrayHelper::map($activeIngridients, 'id', 'name'),
        'showToggleAll' => false,
        'options' => [
            'placeholder' => 'Select ingridients ...',
            'multiple' => true
        ],
        'pluginOptions' => [
            'maximumSelectionLength' => 5
        ],
    ]) ?>
    <br>
    <?= Html::submitButton() ?>
    <?= Html::endForm() ?>
    <br><br>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'label' => Yii::t('app', 'Link'),
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a('View', ['/dish/open', 'slug' => $model['slug']], ['class' => 'btn btn-primary']);
                }
            ]
        ],
    ]); ?>
</div>
