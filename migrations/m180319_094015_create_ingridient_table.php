<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ingridient}}`.
 */
class m180319_094015_create_ingridient_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ingridient}}', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string()->notNull()->unique(),
            'slug' => $this->string()->notNull()->unique(),
            'active' => $this->boolean()->unsigned()->defaultValue(1)->notNull(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);

        $this->createIndex('{{%idx-ingridient-name}}', '{{%ingridient}}', 'name', true);
        $this->createIndex('{{%idx-ingridient-slug}}', '{{%ingridient}}', 'slug', true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('{{%idx-ingridient-name}}', '{{%ingridient}}');
        $this->dropIndex('{{%idx-ingridient-slug}}', '{{%ingridient}}');
        $this->dropTable('{{%ingridient}}');
    }
}
