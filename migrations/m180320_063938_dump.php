<?php

use yii\db\Migration;

/**
 * Class m180320_063938_dump
 */
class m180320_063938_dump extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        exec('mysqldump -u '. $this->db->username . ' -p' . $this->db->password . ' '. $this->getDsnAttribute('dbname') . ' > ' . __DIR__ .'/../dump.sql');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        return true;
    }

    private function getDsnAttribute($name)
    {
        if (preg_match('/' . $name . '=([^;]*)/', $this->db->dsn, $match)) {
            return $match[1];
        } else {
            return null;
        }
    }
}
