<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Ingridient]].
 *
 * @see Ingridient
 */
class IngridientQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['active' => Ingridient::ACTIVE]);
    }

    /**
     * @inheritdoc
     * @return Ingridient[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Ingridient|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
