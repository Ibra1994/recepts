<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Dish */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Dishes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dish-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            [
                'label' => Yii::t('app', 'Ingridients'),
                'format' => 'raw',
                'value' => function ($model) {
                    $links = '';
                    foreach ($model->ingridients as $ingridient) {
                        $links .= ' ' . Html::a($ingridient->name, ['/ingridient/open', 'slug' => $ingridient->slug], ['class' => 'btn btn-sm btn-primary']);
                    }
                    return $links;
                }
            ]

        ],
    ]) ?>

</div>
