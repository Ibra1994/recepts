<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%dish_ingridient}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%dish}}`
 * - `{{%ingridient}}`
 */
class m180319_095224_create_junction_for_dish_and_ingridient_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%dish_ingridient}}', [
            'dish_id' => $this->integer()->unsigned(),
            'ingridient_id' => $this->integer()->unsigned(),
            'PRIMARY KEY(dish_id, ingridient_id)',
        ]);

        // creates index for column `dish_id`
        $this->createIndex(
            '{{%idx-dish_ingridient-dish_id}}',
            '{{%dish_ingridient}}',
            'dish_id'
        );

        // add foreign key for table `{{%dish}}`
        $this->addForeignKey(
            '{{%fk-dish_ingridient-dish_id}}',
            '{{%dish_ingridient}}',
            'dish_id',
            '{{%dish}}',
            'id',
            'CASCADE'
        );

        // creates index for column `ingridient_id`
        $this->createIndex(
            '{{%idx-dish_ingridient-ingridient_id}}',
            '{{%dish_ingridient}}',
            'ingridient_id'
        );

        // add foreign key for table `{{%ingridient}}`
        $this->addForeignKey(
            '{{%fk-dish_ingridient-ingridient_id}}',
            '{{%dish_ingridient}}',
            'ingridient_id',
            '{{%ingridient}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%dish}}`
        $this->dropForeignKey(
            '{{%fk-dish_ingridient-dish_id}}',
            '{{%dish_ingridient}}'
        );

        // drops index for column `dish_id`
        $this->dropIndex(
            '{{%idx-dish_ingridient-dish_id}}',
            '{{%dish_ingridient}}'
        );

        // drops foreign key for table `{{%ingridient}}`
        $this->dropForeignKey(
            '{{%fk-dish_ingridient-ingridient_id}}',
            '{{%dish_ingridient}}'
        );

        // drops index for column `ingridient_id`
        $this->dropIndex(
            '{{%idx-dish_ingridient-ingridient_id}}',
            '{{%dish_ingridient}}'
        );

        $this->dropTable('{{%dish_ingridient}}');
    }
}
