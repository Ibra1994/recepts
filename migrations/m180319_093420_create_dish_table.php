<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%dish}}`.
 */
class m180319_093420_create_dish_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%dish}}', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string()->notNull()->unique(),
            'slug' => $this->string()->notNull()->unique(),
            'created_at' => $this->integer(11)->unsigned(),
        ]);

        $this->createIndex('{{%idx-dish-name}}', '{{%dish}}', 'name', true);
        $this->createIndex('{{%idx-dish-slug}}', '{{%dish}}', 'slug', true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('{{%idx-dish-name}}', '{{%dish}}');
        $this->dropIndex('{{%idx-dish-slug}}', '{{%dish}}');
        $this->dropTable('{{%dish}}');
    }
}
