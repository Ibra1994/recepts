<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user}}`.
 */
class m180319_074300_create_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey()->unsigned(),
            'email' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'access_token' => $this->string(32)->notNull(),
            'password' => $this->string()->notNull()
        ]);

        $this->createIndex('{{%idx-user-email}}', '{{%user}}', 'email', true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('{{%idx-user-email}}', '{{%user}}');
        $this->dropTable('{{%user}}');
    }
}
